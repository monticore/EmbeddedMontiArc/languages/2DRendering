/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.application.application._symboltable;

import de.monticore.ast.ASTNode;
import de.monticore.lang.application.LogConfig;
import de.monticore.modelloader.ModelingLanguageModelLoader;
import de.monticore.symboltable.MutableScope;
import de.monticore.symboltable.ResolvingConfiguration;
import de.monticore.symboltable.SymbolTableCreator;
import de.monticore.symboltable.resolving.CommonResolvingFilter;

import java.util.Optional;

public class ApplicationLanguage extends ApplicationLanguageTOP {
  public static final String FILE_ENDING = "apl";

  public ApplicationLanguage() {
    super("Application Language", FILE_ENDING);
    //LogConfig.init();
    addResolvingFilter(CommonResolvingFilter.create(ApplicationStatementsSymbol.KIND));
  }

  public ApplicationLanguage(String langName, String fileEnding) {
    super(langName, fileEnding);
    }

  @Override
  protected ModelingLanguageModelLoader<? extends ASTNode> provideModelLoader() {
    return new ApplicationModelLoader(this);
  }


  @Override
  public Optional<? extends SymbolTableCreator> getSymbolTableCreator(ResolvingConfiguration resolvingConfiguration, MutableScope enclosingScope) {
    return Optional.of(new ApplicationSymbolTableCreator(resolvingConfiguration, enclosingScope));

  }
}
