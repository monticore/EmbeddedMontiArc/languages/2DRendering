/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.application.application._symboltable;


import de.monticore.lang.application.application._ast.ASTApplicationStatements;
import de.monticore.symboltable.CommonSymbol;

/**
 */
public class ApplicationStatementsSymbol extends CommonSymbol {
    public static ApplicationStatementsSymbolKind KIND = new ApplicationStatementsSymbolKind();

    public ApplicationStatementsSymbol(String name, ASTApplicationStatements ast) {
        super(name, KIND);
        setAstNode(ast);
    }

}
