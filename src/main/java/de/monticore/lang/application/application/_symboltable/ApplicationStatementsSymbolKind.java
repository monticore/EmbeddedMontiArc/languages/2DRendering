/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.application.application._symboltable;

/**
 */
public class ApplicationStatementsSymbolKind implements de.monticore.symboltable.SymbolKind {
    public static final ApplicationStatementsSymbolKind INSTANCE = new ApplicationStatementsSymbolKind();

    public ApplicationStatementsSymbolKind() {

    }
}
