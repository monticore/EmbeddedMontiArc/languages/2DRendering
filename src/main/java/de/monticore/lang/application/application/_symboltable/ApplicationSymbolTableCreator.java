/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.application.application._symboltable;

import de.monticore.lang.application.application._ast.ASTApplicationStatements;
import de.monticore.lang.application.application._visitor.ApplicationVisitor;
import de.monticore.symboltable.MutableScope;
import de.monticore.symboltable.ResolvingConfiguration;
import de.monticore.symboltable.Scope;
import de.se_rwth.commons.logging.Log;

import java.util.Deque;

public class ApplicationSymbolTableCreator extends de.monticore.symboltable.CommonSymbolTableCreator
        implements ApplicationVisitor {

    public ApplicationSymbolTableCreator(
            final ResolvingConfiguration resolvingConfig, final MutableScope enclosingScope) {
        super(resolvingConfig, enclosingScope);
    }

    public ApplicationSymbolTableCreator(final ResolvingConfiguration resolvingConfig, final Deque<MutableScope> scopeStack) {
        super(resolvingConfig, scopeStack);
    }


    /**
     * Creates the symbol table starting from the <code>rootNode</code> and
     * returns the first scope that was created.
     *
     * @param rootNode the root node
     * @return the first scope that was created
     */
    public Scope createFromAST(de.monticore.lang.application.application._ast.ASTApplicationNode rootNode) {
        Log.errorIfNull(rootNode, "0xA7004_244 Error by creating of the ApplicationSymbolTableCreator symbol table: top ast node is null");
        rootNode.accept(realThis);
        return getFirstCreatedScope();
    }

    private ApplicationVisitor realThis = this;

    public ApplicationVisitor getRealThis() {
        return realThis;
    }

    @Override
    public void setRealThis(ApplicationVisitor realThis) {
        if (this.realThis != realThis) {
            this.realThis = realThis;
        }
    }

    @Override
    public void endVisit(de.monticore.lang.application.application._ast.ASTApplicationCompilationUnit ast) {
        setEnclosingScopeOfNodes(ast);
    }

    @Override
    public void visit(ASTApplicationStatements ast) {
        ApplicationStatementsSymbol symbol = new ApplicationStatementsSymbol("ApplicationStatements", ast);

        addToScopeAndLinkWithNode(symbol, ast);
    }


}
