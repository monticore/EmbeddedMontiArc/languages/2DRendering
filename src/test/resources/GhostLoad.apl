// (c) https://github.com/MontiCore/monticore 
//Load textures
if execute
load texture ghost1Texture from "textures/ghost1";
load texture ghost2Texture from "textures/ghost2";
load texture ghost3Texture from "textures/ghost3";
load texture ghost4Texture from "textures/ghost4";

//create sprites
create sprite ghost1;
create sprite ghost2;
create sprite ghost3;
create sprite ghost4;

//add textures to sprites
add texture ghost1Texture to ghost1;
add texture ghost2Texture to ghost2;
add texture ghost3Texture to ghost3;
add texture ghost4Texture to ghost4;

set ghost1 texture delay to 0;//does not advance if it is 0

set ghost2 texture delay to 0;//does not advance if it is 0

set ghost3 texture delay to 0;//does not advance if it is 0

set ghost4 texture delay to 0;//does not advance if it is 0
end
