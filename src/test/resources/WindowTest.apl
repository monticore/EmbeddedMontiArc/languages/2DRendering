// (c) https://github.com/MontiCore/monticore 
set window size to 800 600;
create stage MainStage;
load texture pacman1 from "textures/pacman1";
load texture pacman2 from "textures/pacman2";
load texture pacman3 from "textures/pacman3";

create sprite pacman;
add texture pacman1 to pacman;
add texture pacman2 to pacman;
add texture pacman2 to pacman;
set pacman texture delay to 4;

hide all stages;
add sprite pacman to stage MainStage;
set pacman position to 50 50;
set pacman visible;



set MainStage visible;

set variable moveDown to "s";
set variable moveUp to "w";
set variable moveLeft to "a";
set variable moveRight to "d";

delete sprite "sprite" + "X" + posX + "Y" + posY;
